import { baseUrl } from "../../assets/data.json";

export default function About() {
  const ret = document.createElement("div");
  ret.classList.add("pagewrapper");
  ret.innerHTML = `<div class='page'><img class='banner' src='${baseUrl}/hertzschlag/images/banner.jpg'></img><h1>Impressum</h1>Malte Jürgens<br>Kurfürstenstr. 12<br>52066 Aachen<h3>Kontakt:</h3><a href='mailto:hertzschlag@hhgym.de'>hertzschlag@hhgym.de</a><br><a href='mailto:maltejur@dismail.de'>maltejur@dismail.de</a></div>`;
  return ret;
}
