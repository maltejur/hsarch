import Home from "./modules/home";
import Router from "./modules/router";

new Router(
  document.querySelector("#main")!,
  {
    "": () => Home({ embed: "true" }),
  },
  {
    newUrl: "https://hsarch.hhgym.de",
  }
);
